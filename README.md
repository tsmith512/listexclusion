A WordPress Plugin, based on Exclude Pages from Navigation] (from WP.org),
to exclude pages from wp_list_pages calls for automatically generated navigation
menus. This rewritten version modernizes the plugin for WP3.0+, is more efficient,
changes what I felt was a confusing front-end interface, eliminates and will
continue to grow with input and as needed by clients/agency.

GitHub Repository: https://github.com/tsmith512/listexclusion

### Plugin Declaration from "Exclude Pages from Navigation:
> 
>  Plugin Name: Exclude Pages from Navigation
>
>  Plugin URI: http://wordpress.org/extend/plugins/exclude-pages/
>
>  Description: Provides a checkbox on the editing page which you can check to
>    exclude pages from the primary navigation. IMPORTANT NOTE: This will remove
>    the pages from any "consumer" side page listings, which may not be limited
>    to your page navigation listings.
>
>  Version: 1.92
>
>  Author: Simon Wheatley
>
>  Author URI: http://simonwheatley.co.uk/wordpress/
>
>  Copyright 2007 Simon Wheatley
>
>  This script is free software; you can redistribute it and/or modify
>  it under the terms of the GNU General Public License as published by
>  the Free Software Foundation; either version 3 of the License, or
>  (at your option) any later version.
>
>  This script is distributed in the hope that it will be useful,
>  but WITHOUT ANY WARRANTY; without even the implied warranty of
>  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
>  GNU General Public License for more details.
>
>  You should have received a copy of the GNU General Public License
>  along with this program.  If not, see <http://www.gnu.org/licenses/>.
> 

Exclude Pages on WP.org: http://wordpress.org/extend/plugins/exclude-pages/

Simon Wheatley on the Web: http://simonwheatley.co.uk/wordpress/
